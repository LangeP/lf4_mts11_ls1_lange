import java.util.Scanner;

public class Modulo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.println("Zahlen bis 200, die durch 7 teilbar sind:");
		for (int i = 0; i < 200; i++) {

			if (i % 7 == 0) {

				System.out.printf(i + ", ");
			}
		}
		System.out.println("\nZahlen bis 200, die durch 5 teilbar sind, aber nicht 4:");
		for (int i = 0; i < 200; i++) {

			if (i % 5 == 0 && i % 4 != 0) {

				System.out.printf(i + ", ");
			}
		}

	}

}
