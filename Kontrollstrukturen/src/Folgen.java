
public class Folgen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		System.out.printf("a) ");

		for (int x = 99; x > 9; x -= 3) {

			System.out.print(x + ", ");

		}

		System.out.print("\nb) ");

		for (int x = 1; x <= 20; x++) {

			System.out.print(x * x + ", ");
		}

		System.out.printf("\nc) ");

		for (int x = 2; x <= 102; x += 4) {

			System.out.print(x + ", ");

		}

		System.out.printf("\nd) ");

		for (int x = 1; x <= 16; x++) {

			System.out.print(x * x * 4 + ", ");

		}

		System.out.printf("\ne) ");

		for (int x = 2; x <= 32768; x *= 2) {
			if (x < 32768) {
				System.out.print(x + ", ");
			} else {
				System.out.print(x);
			}
		}
	}
}
