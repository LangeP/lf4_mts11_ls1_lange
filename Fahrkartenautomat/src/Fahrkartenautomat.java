
import java.util.Scanner;

class Fahrkartenautomat {

	static Scanner tastatur = new Scanner(System.in);

	public static double fahrkartenbestellungErfassen() {

		System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus: ");
		System.out.println("  (1) Einzelfahrschein Regeltarif AB [2,90 EUR]");
		System.out.println("  (2) Einzelfahrschein Regeltarif BC [3,30 EUR]");
		System.out.println("  (3) Einzelfahrschein Regeltarif ABC [3,60 EUR]");
		System.out.println("  (4) Kurzstrecke [1,90 EUR]");
		System.out.println("  (5) Tageskarte Regeltarif AB [8,60 EUR]");
		System.out.println("  (6) Tageskarte Regeltarif AC [9,00 EUR]");
		System.out.println("  (7) Tageskarte Regeltarif ABC [9,60 EUR]");
		System.out.println("  (8) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");
		System.out.println("  (9) Kleingruppen-Tageskarte Regeltarif BC [24,30 EUR]");
		System.out.println(" (10) Kleingruppen-Tageskarte Regeltarif ABC [24,90 EUR]\");");

		double ticketpreis = 0.0;
		double[] preisliste = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };

		do {
			System.out.println("Ihre Wahl: ");
			String kartenart = tastatur.next();

			switch (kartenart) {
			case "1":
				ticketpreis = preisliste[0];
				break;
			case "2":
				ticketpreis = preisliste[1];
				break;
			case "3":
				ticketpreis = preisliste[2];
				break;
			case "4":
				ticketpreis = preisliste[3];
				break;
			case "5":
				ticketpreis = preisliste[4];
				break;
			case "6":
				ticketpreis = preisliste[5];
				break;
			case "7":
				ticketpreis = preisliste[6];
				break;
			case "8":
				ticketpreis = preisliste[7];
				break;
			case "9":
				ticketpreis = preisliste[8];
				break;
			case "10":
				ticketpreis = preisliste[9];
				break;
			default:
				System.out.println(">>falsche Eingabe<<");
				break;
			}
		} while (ticketpreis == 0.0);

		System.out.print("Anzahl der Tickets: ");
		double anzahlDerTickets = tastatur.nextDouble();

		return ticketpreis * anzahlDerTickets;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}

	}

	public static void rückgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}

	public static void main(String[] args) {

		while (true) {

			double eingezahlterGesamtbetrag;
			double eingeworfeneMünze;

			double zuZahlenderBetrag = fahrkartenbestellungErfassen();

			// Geldeinwurf
			// -----------
			double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

			// Fahrscheinausgabe
			// -----------------

			fahrkartenAusgeben();

			System.out.println("\n\n");

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------

			rückgeldAusgeben(rückgabebetrag);

			System.out.println("\nVergessen Sie nicht, den/die Fahrschein/-e\n"
					+ "vor Fahrtantritt entwerten zu lassen!\n" + "Wir wünschen Ihnen eine gute Fahrt.");

			System.out.println();
		}
	}
}

