public class Aufgabe1_6_3 {
    public static void main (String[] args) {

        String fahrenheit = "Fahrenheit";
        String celsius = "Celsius";
        String line = "-";
        int frequency = 20;
        int a = -20, b = -10, c = 0, d = 20, e = 30;
        double f = -28.8889, g = -23.3333, h = -17.7778, i = -6.6667, j = -1.1111;

        line = line.repeat(frequency);

        System.out.printf("%s | %6s\n", fahrenheit, celsius);
        System.out.printf("%20s\n", line);
        System.out.printf("%-10d |%8.2f\n", a, f);
        System.out.printf("%-10d |%8.2f\n", b, g);
        System.out.printf("%-10d |%8.2f\n", c, h);
        System.out.printf("%-10d |%8.2f\n", d, i);
        System.out.printf("%-10d |%8.2f\n", e, j);
    }
}
