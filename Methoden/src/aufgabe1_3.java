import java.lang.Math;

public class aufgabe1_3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	System.out.println(cubeVolume(3));
	System.out.println(cuboidVolume(3,5,2));
	System.out.println(pyramideVolume(3,5));
	System.out.println(sphereVolume(3));
	
	}	
	
	public static double cubeVolume (double a) {
		return a*a*a;
	}
	
	public static double cuboidVolume (double a, double b, double c) {
		return a*b*c;
	}
	
	public static double pyramideVolume (double a, double h) {
		return a*a*h/3;
	}
	
	public static double sphereVolume (double r) {
		return 4/3*r*r*r*Math.PI;
	}
}
