import java.util.Scanner;

public class aufgabe1_4 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Abfrage des Gesamtbudgets
		Scanner scanner = new Scanner(System.in);
		String waehrung = "";

		System.out.println("Was ist das Gesamtbudget?");
		double gesamtbudget = scanner.nextDouble();

		while (gesamtbudget > 100) {
			System.out.println("In welchen Land bist du?");
			String land = scanner.next();

			switch (land) {
			case "USA":
				waehrung = "Dollar";
				break;
			case "Japan":
				waehrung = "Yen";
				break;
			case "England":
				waehrung = "Pfund";
				break;
			default:
				System.out.println("Das ist kein legales Land!");
			}

			System.out
					.println("Erstes Land: " + land + "\nW�hrung: " + waehrung + " $\nWie viel � werden gewechselt?:");
			double dollar = scanner.nextDouble();
		}
	}

	public static double euroToDollar(double x) {
		return x * 1.22;
	}

	public static double euroToYen(double x) {
		return x * 126.5;
	}

	public static double euroToPound(double x) {
		return x * 0.89;
	}

	public static double euroToSwissFrank(double x) {
		return x * 1.08;
	}

	public static double euroToSwedishCrowns(double x) {
		return x * 10.10;
	}

}
