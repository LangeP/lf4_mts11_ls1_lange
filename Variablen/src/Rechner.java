import java.util.Scanner; // Import der Klasse Scanner

public class Rechner

{
public static void main(String[] args) // Hier startet das Programm
{
// Neues Scanner-Objekt myScanner wird erstellt
	
Scanner myScanner = new Scanner(System.in);
System.out.print("Bitte geben Sie eine ganze Zahl ein: ");

// Die Variable zahl1 speichert die erste Eingabe
int zahl1 = myScanner.nextInt();
System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");

// Die Variable zahl2 speichert die zweite Eingabe
int zahl2 = myScanner.nextInt();

// Die Addition der Variablen zahl1 und zahl2
// wird der Variable ergebnis zugewiesen.

int ergebnis_add = zahl1 + zahl2;
System.out.print("\n\n\nErgebnis der Addition lautet: ");
System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis_add);

int ergebnis_sub = zahl1 - zahl2;
System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis_sub);

int ergebnis_mul = zahl1 * zahl2;
System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis_mul);

double ergebnis_div = zahl1 / zahl2;
System.out.print("\n\n\nErgebnis des Dividierens lautet: ");
System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis_div);

myScanner.close();
}
}