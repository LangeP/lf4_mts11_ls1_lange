import java.util.Scanner;

public class Aufgabe2_2_2 {

	public static void main(String[] args) {
	
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie Ihren Namen ein: ");
		
		String name = myScanner.next();
		System.out.print("Bitte geben Sie Ihr Alter ein: ");
		
		
		int age = myScanner.nextInt();
		
		System.out.print("Sie hei�en " + name + " und sind " + age + " Jahre alt.");
		
		myScanner.close();
	}

}
